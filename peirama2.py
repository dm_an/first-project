# !/usr/bin/python

"""
!!! SECOND PART !!!
"""

import os
import time
import matplotlib.pyplot as plt
from mininet.net import Mininet
from mininet.node import Controller, OVSKernelSwitch, OVSKernelAP
from mininet.link import TCLink
from mininet.log import setLogLevel, debug, info
from mininet.cli import CLI

import sys
gnet=None

exp_Time=40 #task time	
switch_packet = 'switch_packet.data' 		#
switch_throughput = 'switch_throughput.data'	# Name of file for metrics
car0_packet = 'car0-packet.data'		#
car0_throughput = 'car0-throughput.data'	#

# Implement the graphic function in order to demonstrate the network measurements
# Hint: You can save the measurement in an output file and then import it here
def graphic():
    file_switch_pkt = open('./' + switch_packet, 'r')
    s_packet = file_switch_pkt.readlines()
    file_switch_pkt.close()

    file_switch_thr = open('./' + switch_throughput, 'r')
    s_throughput = file_switch_thr.readlines()
    file_switch_thr.close()

    file_car0_pkt = open('./' + car0_packet, 'r')
    c_packet = file_car0_pkt.readlines()
    file_car0_pkt.close()

    file_car0_thr = open('./' + car0_throughput, 'r')
    c_throughput = file_car0_thr.readlines()
    file_car0_thr.close()

    # initialize some variable to be lists:
    time_ = []
    
    l1 = []
    l2 = []
    t1 = []
    t2 = []
        
    ll1 = []
    ll2 = []
    tt1 = []
    tt2 = []
    
    # scan the rows of the file stored in lines, and put the values into some variables:
    i = 0
    for x in s_packet:    
        p = x.split()
        k=p[1].split(":")
        l1.append(int(k[1]))  
        if len(l1) > 1:
            ll1.append(l1[i] - l1[i - 1])
        i += 1
    
    i = 0
    for x in s_throughput:    
        p = x.split()
        k=p[1].split(":")
        t1.append(int(k[1]))
        if len(t1) > 1:
            tt1.append(t1[i] - t1[i - 1])
        i += 1
    
    i = 0
    for x in c_packet:    
        p = x.split()
        k=p[1].split(":")
        l2.append(int(k[1]))
        if len(l2) > 1:
            ll2.append(l2[i] - l2[i - 1])
        i += 1
    
    i = 0
    for x in c_throughput:    
        p = x.split()
        k=p[5].split(":")
        #print k[1]
        #break 
        t2.append(int(k[1]))
        if len(t2) > 1:
            tt2.append(t2[i] - t2[i - 1])
        i += 1
    
    i = 0
    for x in range(len(ll1)):    
        time_.append(i)
        i = i + 0.5
    
    fig, ax1 = plt.subplots()
    ax2 = ax1.twinx()
    
    ax1.plot(time_, ll1, color='green', label='Received Data (client)', markevery=7, linewidth=2)
    ax1.plot(time_, ll2, color='red', label='Transmited Data (server)', markevery=7, linewidth=2)
    ax2.plot(time_, tt1, color='magenta', label='Throughput (client)', markevery=7, linewidth=2)
    ax2.plot(time_, tt2, color='black', label='Throughput (server)', markevery=7, linewidth=2)
    ax1.legend(loc=2, borderaxespad=0., fontsize=12)
    ax2.legend(loc=1, borderaxespad=0., fontsize=12)

    ax2.set_yscale('log')

    ax1.set_ylabel("# Packets (unit)", fontsize=18)
    ax1.set_xlabel("Time (seconds)", fontsize=18)
    ax2.set_ylabel("Throughput (bytes/sec)", fontsize=18)

    plt.show()
    plt.savefig("graphic.eps")



def apply_experiment(car,client,switch):
    
    time.sleep(2)
    print "Applying first phase"
    print "Bicasting eNodeB1-rsu1"

    ################################################################################ 
    os.system('ovs-ofctl add-flow switch in_port=1,actions=output:4')
    os.system('ovs-ofctl mod-flows switch in_port=4,actions=output:1')
    os.system('ovs-ofctl add-flow switch in_port=3,actions=output:4')
    os.system('ovs-ofctl mod-flows switch in_port=4,actions=output:3')
    os.system('ovs-ofctl mod-flows switch in_port=2,actions=drop')
    os.system('ovs-ofctl del-flows eNodeB1')
    os.system('ovs-ofctl del-flows eNodeB2')
    os.system('ovs-ofctl del-flows rsu1')
    
    #print "CLI between bicasting"
    #CLI(gnet)
    
    print "Bicasting eNodeB2-rsu1"
    os.system('ovs-ofctl add-flow switch in_port=2,actions=output:4')
    os.system('ovs-ofctl mod-flows switch in_port=4,actions=output:2')
    os.system('ovs-ofctl add-flow switch in_port=3,actions=output:4')
    os.system('ovs-ofctl mod-flows switch in_port=4,actions=output:3')
    os.system('ovs-ofctl mod-flows switch in_port=1,actions=drop')
    os.system('ovs-ofctl del-flows eNodeB1')
    os.system('ovs-ofctl del-flows eNodeB2')
    os.system('ovs-ofctl del-flows rsu1')
       


    #car[0].cmd('ip route add 200.0.10.2 via 200.0.10.50')
    #client.cmd('ip route add 200.0.10.100 via 200.0.10.150')
    
    
    timeout = time.time() + exp_Time
    currentTime = time.time()
    i = 0
    while True:
        if time.time() > timeout:
            break;
        if time.time() - currentTime >= i:
            car[0].cmd('ifconfig bond0 | grep \"TX packets\" >> %s' % car0_packet) 
            client.cmd('ifconfig client-eth0 | grep \"RX packets\" >> %s' %	switch_packet) 
            car[0].cmd('ifconfig bond0 | grep \"bytes\" >> %s' % car0_throughput) 
            client.cmd('ifconfig client-eth0 | grep \"bytes\" >> %s' % switch_throughput) 
            i += 0.5

    
    print "Moving nodes"
    car[0].moveNodeTo('150,100,0')
    car[1].moveNodeTo('120,100,0')
    car[2].moveNodeTo('90,100,0')
    car[3].moveNodeTo('70,100,0')
    
    
    
    #print "after first phase"
    #CLI(gnet)
    

    time.sleep(2)
    print "Applying second phase"
    print "Connecting to eNodeB2"

    ################################################################################ 
    os.system('ovs-ofctl mod-flows switch in_port=2,actions=output:4')
    os.system('ovs-ofctl mod-flows switch in_port=4,actions=output:2')
    os.system('ovs-ofctl mod-flows switch in_port=3,actions=drop')
    os.system('ovs-ofctl mod-flows switch in_port=1,actions=drop')

    os.system('ovs-ofctl del-flows eNodeB1')
    os.system('ovs-ofctl del-flows eNodeB2')
    os.system('ovs-ofctl del-flows rsu1')
      
    

    #car[0].cmd('ip route del 200.0.10.2 via 200.0.10.50')
    #client.cmd('ip route del 200.0.10.100 via 200.0.10.150')
    

    timeout = time.time() + exp_Time
    currentTime = time.time()
    i = 0
    while True:
        if time.time() > timeout:
            break;
        if time.time() - currentTime >= i:
            car[0].cmd('ifconfig bond0 | grep \"TX packets\" >> %s' % car0_packet) 		
            client.cmd('ifconfig client-eth0 | grep \"RX packets\" >> %s' % switch_packet) 	
            car[0].cmd('ifconfig bond0 | grep \"bytes\" >> %s' % car0_throughput) 	
            client.cmd('ifconfig client-eth0 | grep \"bytes\" >> %s' % switch_throughput) 	
            i += 0.5

    
    print "Moving nodes"
    car[0].moveNodeTo('190,100,0')
    car[1].moveNodeTo('150,100,0')
    car[2].moveNodeTo('120,100,0')
    car[3].moveNodeTo('90,100,0')
    
    #print "***CLI after second phase"
    #CLI(gnet)
    
    
def topology():
    "Create a network."
    net = Mininet(controller=Controller, link=TCLink, switch=OVSKernelSwitch, accessPoint=OVSKernelAP)
    global gnet
    gnet = net

    print "*** Creating nodes"
    car = []
    stas = []
    for x in range(0, 4):
        car.append(x)
        stas.append(x)
    for x in range(0, 4):
        car[x] = net.addCar('car%s' % (x), wlans=2, ip='10.0.0.%s/8' % (x + 1), \
        mac='00:00:00:00:00:0%s' % x, mode='b')


    
    eNodeB1 = net.addAccessPoint('eNodeB1', ssid='eNodeB1', dpid='1000000000000000', mode='ac', channel='1', position='80,75,0', range=60)
    eNodeB2 = net.addAccessPoint('eNodeB2', ssid='eNodeB2', dpid='2000000000000000', mode='ac', channel='6', position='180,75,0', range=70)
    rsu1 = net.addAccessPoint('rsu1', ssid='rsu1', dpid='3000000000000000', mode='g', channel='11', position='140,120,0', range=40)
    c1 = net.addController('c1', controller=Controller)
    client = net.addHost ('client')
    switch = net.addSwitch ('switch', dpid='4000000000000000')

    net.plotNode(client, position='125,230,0')
    net.plotNode(switch, position='125,200,0')

    print "*** Configuring wifi nodes"
    net.configureWifiNodes()

    print "*** Creating links"
    net.addLink(eNodeB1, switch)
    net.addLink(eNodeB2, switch)
    net.addLink(rsu1, switch)
    net.addLink(switch, client)
    

    print "*** Starting network"
    net.build()
    c1.start()
    eNodeB1.start([c1])
    eNodeB2.start([c1])
    rsu1.start([c1])
    switch.start([c1])

    for sw in net.vehicles:
        sw.start([c1])

    """   
    i = 1
    j = 2
    for c in car:
        c.cmd('ifconfig %s-wlan0 192.168.0.%s/24 up' % (c, i))
        c.cmd('ifconfig %s-eth0 192.168.1.%s/24 up' % (c, i))
        c.cmd('ip route add 10.0.0.0/8 via 192.168.1.%s' % j)
        i += 2
        j += 2

    i = 1
    j = 2
    for v in net.vehiclesSTA:
        v.cmd('ifconfig %s-eth0 192.168.1.%s/24 up' % (v, j))
        v.cmd('ifconfig %s-mp0 10.0.0.%s/24 up' % (v, i))
        v.cmd('echo 1 > /proc/sys/net/ipv4/ip_forward')
        i += 1
        j += 2

    for v1 in net.vehiclesSTA:
        i = 1
        j = 1
        for v2 in net.vehiclesSTA:
            if v1 != v2:
                v1.cmd('route add -host 192.168.1.%s gw 10.0.0.%s' % (j, i))
            i += 1
            j += 2 """

    client.cmd('ifconfig client-eth0 200.0.10.2')
    #net.vehiclesSTA[0].cmd('ifconfig car0STA-eth0 200.0.10.50')

    car[0].cmd('modprobe bonding mode=3')
    car[0].cmd('ip link add bond0 type bond')
    car[0].cmd('ip link set bond0 address 02:01:02:03:04:08')
    car[0].cmd('ip link set car0-eth0 down')
    car[0].cmd('ip link set car0-eth0 address 00:00:00:00:00:11')
    car[0].cmd('ip link set car0-eth0 master bond0')
    car[0].cmd('ip link set car0-wlan0 down')
    car[0].cmd('ip link set car0-wlan0 address 00:00:00:00:00:15')
    car[0].cmd('ip link set car0-wlan0 master bond0')
    car[0].cmd('ip link set car0-wlan1 down')
    car[0].cmd('ip link set car0-wlan1 address 00:00:00:00:00:13')
    car[0].cmd('ip link set car0-wlan1 master bond0')
    car[0].cmd('ip addr add 200.0.10.100/24 dev bond0')
    car[0].cmd('ip link set bond0 up')

    #car[3].cmd('ifconfig car3-wlan0 200.0.10.150')

    #client.cmd('ip route add 192.168.1.8 via 200.0.10.150')
    #client.cmd('ip route add 10.0.0.1 via 200.0.10.150')

    #net.vehiclesSTA[3].cmd('ip route add 200.0.10.2 via 192.168.1.7')
    #net.vehiclesSTA[3].cmd('ip route add 200.0.10.100 via 10.0.0.1')
    #net.vehiclesSTA[0].cmd('ip route add 200.0.10.2 via 10.0.0.4')

    #car[0].cmd('ip route add 10.0.0.4 via 200.0.10.50')
    #car[0].cmd('ip route add 192.168.1.7 via 200.0.10.50')
    #car[0].cmd('ip route add 200.0.10.2 via 200.0.10.50')
    #car[3].cmd('ip route add 200.0.10.100 via 192.168.1.8')

    """plot graph"""
    net.plotGraph(max_x=250, max_y=250)

    net.startGraph()

    os.system('rm *.data')

    # Uncomment and modify the two commands below to stream video using VLC 
    car[0].cmdPrint("cvlc -vvv bunnyMob.mp4 --sout '#duplicate{dst=rtp{dst=200.0.10.2,port=5004,,mux=ts},dst=display}' :sout-keep &")
    client.cmdPrint("cvlc rtp://@200.0.10.2:5004 &")
    
    

    car[0].moveNodeTo('110,100,0')
    car[1].moveNodeTo('80,100,0')
    car[2].moveNodeTo('65,100,0')
    car[3].moveNodeTo('50,100,0')

    os.system('ovs-ofctl del-flows switch')

    time.sleep(3)

    apply_experiment(car,client,switch)
    
    
    
    print "*** Generating graphic"
    graphic() 

    # kills all the xterms that have been opened
    os.system('pkill xterm')

    print "*** Running CLI"
    CLI(gnet)
  
   
    os.system('rm *.data')
    os.system('rm *.data')

    print "*** Stopping network"
    net.stop()

if __name__ == '__main__':
    setLogLevel('info')
    try:
        topology()
    except:
        type = sys.exc_info()[0]
        error = sys.exc_info()[1]
        traceback = sys.exc_info()[2]
        print ("Type: %s" % type)
        print ("Error: %s" % error)
        print ("Traceback: %s" % traceback)
        if gnet != None:
            gnet.stop()
        else:
            print "No network was created..."
